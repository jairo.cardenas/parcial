package com.example.Parcial;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListTestJairoParcial {

	private List<String> students;
	private List<String> listadoPrueba;

	public ListTestJairoParcial() {

	}

	@BeforeEach
	void init() {
		this.students = new ArrayList<>(Arrays.asList("Lorena", "Jairo", "Christopher", "Sergio", "Carlos", null));
		this.listadoPrueba = new ArrayList<>(Arrays.asList("Lorena", "Jairo", "Christopher", "Sergio", "Carlos", null));
		;
	}

	@Test
	void testAddStudent() {
		students.add("Altagracia");
		assertEquals("Altagracia", students.get(students.size() - 1));
	}

	@Test
	void testSearchStudent() {
		String result = "Lorena u";
		String expected = students.get(2);
		assertNotSame(expected, result);
	}

	@Test
	void testDeleteAllStudents() {
		students.clear();
		assertTrue(students.isEmpty());
	}

	@Test
	void testCompareObjceArray() {
		List<String> expected = new ArrayList();
		List<String> result = new ArrayList();

		expected.addAll(this.students);
		result.addAll(this.listadoPrueba);

		assertNotSame(expected, result);
	}

	@Test
	void testCompareObjcetNull() {
		String expected = students.get(5);
		assertNull(expected);
	}

	@Test
	void testCompareArrayNumericos() {
		int[] expected = { 1, 2, 3 };
		int[] result = { 1, 2, 3 };
		assertArrayEquals(expected, result);
	}

}
